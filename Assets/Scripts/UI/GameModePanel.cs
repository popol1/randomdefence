using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameModePanel : MonoBehaviour
{


    private void Update()
    {
        if(RoundManager.instance.isGameModeSelected) 
            gameObject.SetActive(false);
    }

    public void SetGameMode(int modeCount)
    {
        switch (modeCount)
        {
            case 0:
                RoundManager.instance.gameMode = GameMode.Easy;
            break;
            case 1:
                RoundManager.instance.gameMode = GameMode.Normal;
                break;
            case 2:
                RoundManager.instance.gameMode = GameMode.Hard;
                break;
        }
        RoundManager.instance.isGameModeSelected = true;
    }
}
