using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 인게임 UI입니다
/// </summary>
public class GameUI : MonoBehaviour
{
    /// <summary>
    /// 선택된 유닛의 정보를 표시하는 패널입니다.
    /// </summary>
    public UnitStatePanel unitStatePanel { get; private set; }

    /// <summary>
    /// 플레이어의 자원을 표시하는 패널입니다.
    /// </summary>
    public ResourcePanel resourcePanel { get; private set; }

    /// <summary>
    /// 플레이어 유닛의 행동 패널 입니다.
    /// </summary>
    public ActionPanel actionPanel { get; private set; }

    /// <summary>
    /// 현재 라운드의 상황을 표시하는 패널입니다.
    /// </summary>
    public RoundPanel roundPanel{ get; private set; }

    /// <summary>
    /// 게임 난이도를 정할 패널입니다.
    /// </summary>
    public GameModePanel gameModePanel { get; private set; }

    /// <summary>
    /// 현재 몬스터의 수 , 패배 조건 등을 표시하는 패널입니다.
    /// </summary>
    public GameStatusPanel monsterCountPanel { get; private set; }

    private void Awake()
    {
        unitStatePanel = GetComponentInChildren<UnitStatePanel>();

        resourcePanel = GetComponentInChildren<ResourcePanel>();

        actionPanel = GetComponentInChildren<ActionPanel>();

        roundPanel = GetComponentInChildren<RoundPanel>();

        gameModePanel = GetComponentInChildren<GameModePanel>();

        monsterCountPanel = GetComponentInChildren<GameStatusPanel>();
    }
}
