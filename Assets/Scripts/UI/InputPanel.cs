using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UIElements;

public sealed class InputPanel : MonoBehaviour
{
    private EventTrigger _EventTrigger;

    private void Awake()
    {
        _EventTrigger = GetComponent<EventTrigger>();

        EventTrigger.Entry onClickEvent = new EventTrigger.Entry();
        onClickEvent.eventID = EventTriggerType.PointerDown;
        onClickEvent.callback.AddListener(CALLBACK_OnInputPanelClicked);
        _EventTrigger.triggers.Add(onClickEvent);
    }

    private void CALLBACK_OnInputPanelClicked(BaseEventData evtData)
    {
        PointerEventData pointerEventData = (evtData as PointerEventData);

    }

    

}
