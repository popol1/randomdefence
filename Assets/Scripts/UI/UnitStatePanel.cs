using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// 유닛의 정보를 표시하는 클래스입니다.
/// </summary>
public class UnitStatePanel : MonoBehaviour
{
    /// <summary>
    /// " / " 글자입니다.
    /// </summary>
    [SerializeField]
    private TMP_Text _Slash;

    [SerializeField]
    /// <summary>
    /// 표기할 유닛의 최대체력
    /// </summary>
    private TMP_Text _MaxHp;

    [SerializeField]
    /// <summary>
    /// 표기할 유닛의 현재 체력
    /// </summary>
    private TMP_Text _CurrentHp;

    [SerializeField]
    /// <summary>
    /// 유닛의 아이콘 이미지
    /// </summary>
    private Image _UnitIcon;

    [SerializeField]
    /// <summary>
    /// 유닛의 공격력
    /// </summary>
    private TMP_Text _UnitAtk;

    private ObjectBase _SelectUnit;

    /// <summary>
    /// 여러 유닛들이 선택될때 표시되는 패널입니다.
    /// </summary>
    public GameObject _UnitsStatePanel;

    /// <summary>
    /// 유닛의 기본 정보를 표시하는 패널입니다.
    /// </summary>
    public GameObject _StatePanel;

    /// <summary>
    /// 유닛의 공격력 , 방어력 을 표시하는 패널입니다.
    /// </summary>
    public GameObject _BattleInfoPanel;

    /// <summary>
    /// 선택된 유닛들을 담는 리스트입니다.
    /// </summary>
    public List<PlayerUnit> _PlayerUnitList;

    /// <summary>
    /// 유닛들의 아이콘을 나열할 이미지 배열입니다.
    /// </summary>
    public Image[] m_GridUnitIcon;

    /// <summary>
    /// 1개의 유닛만 선택될때 유닛 패널을 세팅합니다.
    /// </summary>
    /// <param name="targetUnit">표시할 유닛을 전달받습니다.</param>
    public void SetUnitStatePanel(ObjectBase targetUnit)
    {
        gameObject.SetActive(true);
        ResetIcon();
        _SelectUnit = targetUnit;
        if (targetUnit.objectData.m_ObjectType == ObjectTypes.Unit)
        {
            UnitBase unit = (UnitBase)targetUnit;
            SetUnitInfoPanel(unit);

        }

        _StatePanel.SetActive(true);
        _BattleInfoPanel.SetActive(true);
    }

    /// <summary>
    /// 여러 유닛들을 선택할때 유닛 리스트 패널을 세팅합니다.
    /// </summary>
    /// <param name="unitList">선택된 유닛들의 리스트를 전달받습니다.</param>
    public void SetUnitListStatePanel(List<PlayerUnit> unitList)
    {
        gameObject.SetActive(true);
        ResetIcon();

        _PlayerUnitList.AddRange(unitList);
        SetUnitInfoPanel(unitList[0]);

        _StatePanel.SetActive(true);
        _UnitsStatePanel.SetActive(true);

        for(int i = 0; i< unitList.Count; i++)
        {
            m_GridUnitIcon[i].sprite = unitList[i].objectData.m_ObjectIcon;
            m_GridUnitIcon[i].gameObject.SetActive(true);
        }


    }

    /// <summary>
    /// 패널들을 초기값으로 초기화 시킵니다.
    /// </summary>
    private void ResetIcon()
    {
        _BattleInfoPanel.SetActive(false);
        _StatePanel.SetActive(false);
        _UnitsStatePanel.SetActive(false);
        _PlayerUnitList.Clear();
        foreach(Image unitIcon in m_GridUnitIcon)
        {
            unitIcon.sprite = null;
            unitIcon.gameObject.SetActive(false);
        }
    }

    /// <summary>
    /// 기본 세팅값들을 선택된 유닛의 정보로 갱신합니다.
    /// </summary>
    /// <param name="unit"></param>
    private void SetUnitInfoPanel(UnitBase unit)
    {
        _UnitIcon.sprite = unit.objectData.m_ObjectIcon;
        _MaxHp.text = unit.unitData.m_MaxHp.ToString();
        _CurrentHp.text = unit.m_CurrentHp.ToString();
        _UnitAtk.text = unit.unitData.m_Atk.ToString();
    }
}
