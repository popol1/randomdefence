using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

/// <summary>
/// 현재 라운드 상황을 표시하는 패널 클래스입니다.
/// </summary>
public class RoundPanel : MonoBehaviour
{
    /// <summary>
    /// 현재 라운드를 표시합니다.
    /// </summary>
    public TMP_Text m_Round;

    /// <summary>
    /// 라운드의 남은 시간을 표시합니다.
    /// </summary>
    public TMP_Text m_Timer;

    private void Start()
    {
        Debug.Log(RoundManager.instance.currentRound);
    }

    private void Update()
    {
        if (RoundManager.instance.currentRound % 10 == 0)
            m_Round.text = "보스 출현 시간:";
        else
            m_Round.text = RoundManager.instance.currentRound.ToString() + " 라운드";
        m_Timer.text = RoundManager.instance.minitue.ToString("00") + ":" + RoundManager.instance.second.ToString("00");
    }


}
