using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ResourcePanel : MonoBehaviour
{
    /// <summary>
    /// 플레이어가 현재 보유중인 골드를 표시할 텍스트입니다.
    /// </summary>
    [SerializeField]
    private TMP_Text _Gold;

    /// <summary>
    /// 플레이어가 현재 보유중인 목재를 표시할 텍스트입니다.
    /// </summary>
    [SerializeField]
    private TMP_Text _Wood;

    /// <summary>
    /// 플레이어가 현재 보유중인 특성 포인트를 표시할 텍스트입니다.
    /// </summary>
    [SerializeField]
    private TMP_Text _LevelPoint;

    /// <summary>
    /// 자원 텍스트를 갱신합니다.
    /// </summary>
    /// <param name="gold">골드를 받아옵니다</param>
    /// <param name="wood">목재를 받아옵니다.</param>
    /// <param name="levelPoint">레벨 포인트를 받아옵니다.</param>
    public void SetResourceText(int gold, int wood , int levelPoint)
    {
        _Gold.text = gold.ToString();
        _Wood.text = wood.ToString();
        _LevelPoint.text = levelPoint.ToString();
    }


}
