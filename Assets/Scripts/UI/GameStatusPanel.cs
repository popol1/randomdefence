using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GameStatusPanel : MonoBehaviour
{
    public TMP_Text m_GameOverDeatailText;

    public TMP_Text m_GameModeText;

    public TMP_Text m_UnitCountText;

    private int _UnitCount;
    private string _GameMode;
    private bool _IsSetUI;

    private void Update()
    {
        if(RoundManager.instance.isGameModeSelected && !_IsSetUI)
            SetInitializeGameModeUI();

        m_UnitCountText.text = $"현재 유닛 카운트 : {RoundManager.instance.currentMonsterCount}";
    }

    public void SetInitializeGameModeUI()
    {
        
        switch(RoundManager.instance.gameMode)
        {
            case GameMode.Easy:
                _UnitCount = Constants.MODE_EASY_UNITCOUNT;
                _GameMode = "쉬움";
                break;
            case GameMode.Normal:
                _UnitCount = Constants.MODE_NORMAL_UNITCOUNT;
                _GameMode = "보통";
                break;
            case GameMode.Hard:
                _UnitCount = Constants.MODE_HARD_UNITCOUNT;
                _GameMode = "어려움";
                break;
        }
        m_GameOverDeatailText.text = $"유닛 카운트 = {_UnitCount} <- 패배 ";
        m_GameModeText.text = $"난이도: {_GameMode} 모드";
        _IsSetUI = true;
    }

}
