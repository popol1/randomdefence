using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// 선택된 유닛의 행동을 정하는 패널입니다.
/// </summary>
public class ActionPanel : MonoBehaviour
{
    public GameObject m_ActionButtonGridParent;

    private PlayerUnit _PlayerUnit;

    public Button[] _GridButtons;

    private void Awake()
    {
        _GridButtons = m_ActionButtonGridParent.GetComponentsInChildren<Button>();
    }

    public void SetToolTip(PlayerUnit playerUnit)
    {
        _PlayerUnit = playerUnit;
    }
}
