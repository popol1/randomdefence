using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : GameManagerBase
{
    protected override void OnGameManagerInitialized()
    {
        base.OnGameManagerInitialized();

        RegisterManager<RoundManager>();
    }

}
