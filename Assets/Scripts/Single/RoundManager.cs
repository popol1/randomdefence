using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoundManager : ManagerClassBase<RoundManager>
{
    public int currentRound = 1;

    public int currentMonsterCount;
    public float roundTime;

    public int second;
    public int minitue;


    public bool isWaiting;

    public GameMode gameMode;
    public bool isGameModeSelected;

    private void Awake()
    {
        roundTime = Constants.DEFAULT_ROUNDTIME;
    }

    private void Update()
    {
        Timer();
    }

    private void Timer()
    {
        if (!isWaiting && roundTime > 0)
        {

            roundTime -= Time.deltaTime;
            if (roundTime <= 0)
            {
                Debug.Log("�ð� ����");
                currentRound++;
                roundTime = Constants.DEFAULT_ROUNDTIME;
            }
            
        }
        minitue = (int)roundTime / 60;
        second = (int)roundTime % 60;
    }
}
