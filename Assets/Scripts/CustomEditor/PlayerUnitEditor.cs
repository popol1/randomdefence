using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;

[CustomEditor(typeof(PlayerUnit))]
[CanEditMultipleObjects]
public class PlayerUnitEditor : Editor
{
    bool showPlayerStatGroup = true;
    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        PlayerUnit myComponent = (PlayerUnit)target;


        SerializedProperty property = serializedObject.GetIterator();
        bool enterChildren = true;

        while (property.NextVisible(enterChildren))
        {
            enterChildren = false;
            // 특정 속성을 건너뛰기 위해 조건을 사용
            if (property.name == "_CreateRec" || property.name == "_UnitData" || property.name == "_ObjectData")
                continue;

            EditorGUILayout.PropertyField(property, true);
        }

        // UnitTech가 Unused , Highest , Selectable 이 아닐경우 조합법 표시
        if (myComponent.m_UnitTech != UnitTech.Unused &&
            myComponent.m_UnitTech != UnitTech.Highest&&
            myComponent.m_UnitTech != UnitTech.Selectable)
        {
            EditorGUILayout.PropertyField(serializedObject.FindProperty("_CreateRec"), new GUIContent("CreateRec"));
        }


        showPlayerStatGroup = EditorGUILayout.Foldout(showPlayerStatGroup, "Player Stat");

        if(showPlayerStatGroup)
        {
            GUILayout.Space(10);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("_UnitData"), true);
            GUILayout.Space(10);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("_ObjectData"), true);
            GUILayout.Space(10);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("_CreateRec"), true);
        }
        serializedObject.ApplyModifiedProperties();  
    }

}
