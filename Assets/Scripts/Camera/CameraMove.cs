using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public class CameraMove : MonoBehaviour
{
    [Header("# 메인 카메라")]
    public Camera m_MainCamera;

    [Header("# 카메라 이동 속도")]
    public float m_MoveSpeed;

    [Header("# 마우스 커서 오브젝트")]
    public Image m_MouseCursor;

    private float _PanBorderThickness = 10.0f;

    /// <summary>
    /// 카메라가 화면 밖으로 나가지 못하도록 가두는 변수
    /// </summary>
    private float _MaxHeight = Screen.height;
    private float _MaxWidth = Screen.width;


    public RectTransform rt;


    private void Update()
    {
        CameraMovement();
        UpdateCursorPosition();
    }


    /// <summary>
    /// 
    /// </summary>
    private void UpdateCursorPosition()
    {
        Vector3 pos = Mouse.current.position.ReadValue();

        // 스크린 좌표를 UI 요소의 로컬 좌표로 변환
        Vector2 localPoint;
        RectTransformUtility.ScreenPointToLocalPointInRectangle(
            rt,
            pos,
            null, // 사용할 카메라
            out localPoint // 변환된 로컬 좌표를 저장할 변수
        );
        float clampedX = Mathf.Clamp(pos.x, _PanBorderThickness, _MaxWidth - _PanBorderThickness);
        float clampedY = Mathf.Clamp(pos.y, _PanBorderThickness, _MaxHeight - _PanBorderThickness);

        Vector2 currentPos = new Vector2(clampedX, clampedY);

        // 변환된 로컬 좌표를 UI 요소의 위치로 설정
        m_MouseCursor.rectTransform.anchoredPosition = currentPos;
    }

    /// <summary>
    /// 카메라를 움직입니다.
    /// </summary>
    private void CameraMovement()
    {
        Vector3 pos = m_MainCamera.transform.position;

        // 커서가 화면 아래쪽 끝에 닿았을때
        if(Mouse.current.position.ReadValue().y >= _MaxHeight - _PanBorderThickness)
        {
            pos.z += m_MoveSpeed * Time.deltaTime;
        }

        // 커서가 화면 위에 닿았을때
        if(Mouse.current.position.ReadValue().y <= _PanBorderThickness)
        {
            pos.z -= m_MoveSpeed * Time.deltaTime;
        }
        
        // 커서가 화면 오른쪽 끝에 닿았을때
        if(Mouse.current.position.ReadValue().x >= _MaxWidth - _PanBorderThickness)
        {
            pos.x += m_MoveSpeed * Time.deltaTime;
        }

        // 커서가 화면 왼쪽 끝에 닿았을때
        if(Mouse.current.position.ReadValue().x <= _PanBorderThickness)
        {
            pos.x -= m_MoveSpeed * Time.deltaTime;
        }

        m_MainCamera.transform.position = pos;
    }

}
