
using System;
using System.Collections.Generic;
using UnityEngine;

public static class ListExtensions
{
    public static bool IsEmpty<T>(this List<T> list)
    {
        return list == null || list.Count == 0;
    }

}

public static class IntExtensions
{
    public static bool CheckPercent(this int origin, int percent)
    {
        if (percent < 0 || percent > 100)
        {
            throw new ArgumentOutOfRangeException(nameof(percent), "퍼센트는 0과 100 사이여야 합니다.");
        }

        return origin >= percent;
    }
}

public static class ArrayExtensions
{
    public static T[] GetComponentsInColliders<T> (this Collider[] colliders) where T : Component
    {
        List<T> components = new();

        foreach (Collider collider in colliders)
        {
            T component = collider.GetComponent<T>();
            if (component != null)
            {
                components.Add(component);
            }
        }

        return components.ToArray();
    }

    public static bool IsArrayEmpty<T>(this T[] array)
    {
        return array == null || array.Length == 0;
    }
}

public static class FloatExtensions
{
    public static Vector3 FloatToVector3(this float x)
    {
        return new Vector3(x, x, x);
    }
}