using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu (menuName = "ScriptableObject/ObjectData")]
public class ObjectStatDataScriptableObject : ScriptableObject
{
    public List<ObjectDataElem> m_ObjectDatas;

    public ObjectData GetObjectData(string ObjectCode)
        => m_ObjectDatas.Find(elem => elem.m_ObjectCode == ObjectCode).m_ObjectData;
}

[Serializable]
public class ObjectDataElem 
{
    public string m_ObjectCode;

    public ObjectData m_ObjectData;
}

[Serializable]
public struct ObjectData
{
    [Header("# 유닛 이름")]
    public string m_ObjectName;

    [Header("# 유닛 아이콘")]
    public Sprite m_ObjectIcon;

    [Header("# 방어 가능 여부")]
    public DefTypes m_DefType;

    [Header("# 오브젝트 타입")]
    public ObjectTypes m_ObjectType;
}