using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName = "ScriptableObject/UnitCreateRec")]
public class UnitCreateRecScriptableObject : ScriptableObject
{
    public List<CreateRecElem> createRecElems;

    public CreateRec GetCreateRec(string RecCode)
        => createRecElems.Find(elem => elem._RecCode == RecCode)._Rec;
}

[Serializable]
public class CreateRecElem
{
    public string _RecCode;
    public CreateRec _Rec;
}

[Serializable]
public struct CreateRec
{
    public PlayerUnit[] _Units;

    public PlayerUnit _Result;
}