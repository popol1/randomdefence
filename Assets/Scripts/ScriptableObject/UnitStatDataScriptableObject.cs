using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName = "ScriptableObject/UnitStatData")]
/// <summary>
/// 유닛의 데이터를 관리하는 ScriptableObject입니다
/// </summary>
public class UnitStatDataScriptableObject : ScriptableObject
{
    public List<UnitStatData> data;

    public UnitStatData GetUnitData(string unitName)
        => data.Find(elem => elem.m_UnitCode == unitName);

}

[Serializable]
public class UnitStatData
{
    /// <summary>
    /// 유닛의 최대 체력
    /// </summary>
    public float m_MaxHp;

    /// <summary>
    /// 유닛의 공격력
    /// </summary>
    public float m_Atk;

    /// <summary>
    /// 유닛의 방어력
    /// </summary>
    public float m_Def;

    public string m_UnitCode;

    public List<string> m_SkillCodeList;

}