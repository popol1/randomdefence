using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "ScriptableObject/SkillData")]
public class SkillDataScriptableObject : ScriptableObject
{
    public List<SkilDataElem> skilDataElems;

    public SkillData GetSkillData(string skillCode)
        => skilDataElems.Find(elem => elem.m_SkillCode == skillCode).skilDataElems;

}

[Serializable]
public class SkilDataElem
{
    public string m_SkillCode;

    public SkillData skilDataElems;
}

[Serializable]
public class SkillData
{
    [Header("��ų ����Ʈ")]
    public GameObject m_SkillEffect;

    public Sprite m_SkillIcon;

    [TextArea]
    public string m_SkillInfo;

    public string m_SkillName;

    public SkillDebuffTypes m_SkillDebuffTypes;

    public TargetType m_TargetTypes;

    public float m_SlowAmount;

    public float m_Damage;

    public int m_Percent;

    public float m_Range;

    public float m_DebuffTime;

}
