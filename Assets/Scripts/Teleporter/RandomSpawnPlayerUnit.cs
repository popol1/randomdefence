using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class RandomSpawnPlayerUnit : MonoBehaviour
{
    public Transform m_SpawnPos;

    public PlayerUnit[] m_PlayerUnits;


    private PlayerCharacter _PlayerCharacter;
    public void RandomSpawn()
    {
        int randomIndex = Random.Range(0, m_PlayerUnits.Length -1);

        PlayerUnit spawnUnit = m_PlayerUnits[randomIndex];

        Instantiate(spawnUnit.gameObject, m_SpawnPos.position, Quaternion.identity);

        GameSceneInstance sceneInstance = SceneManagerBase.instance.GetSceneInstance<GameSceneInstance>();
        _PlayerCharacter = sceneInstance.playerController.controlledCharacter as PlayerCharacter;

        _PlayerCharacter.OnAddUnitDic(spawnUnit.m_ObjectCode, spawnUnit);
    }

}
