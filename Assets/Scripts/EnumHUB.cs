


public enum ObjectTypes
{
    Structure,          // 건물 타입
    Unit,               // 유닛 타입
}

public enum DefTypes
{
    Unbreakable,        // 공격받지 않는타입
    Breakable           // 공격 받는 타입
}

public enum UnitTech
{
    Normal,     // 노멀   유닛
    Rare,       // 레어   유닛
    Unique,     // 유니크 유닛
    Highest,    // 최상위 유닛
    Unused,     // 조합법이 필요하지 않은 유닛 (이미 다른 유닛들이 조합법을 다 갖고있음)
    Selectable  // 뽑기 전용 유닛
}

public enum GameMode
{
    Easy,       // 이지 모드
    Normal,     // 노말 모드    
    Hard        // 하드 모드
}

/// <summary>
/// 스킬의 타겟 타입을 나타냅니다.
/// </summary>
public enum TargetType
{
    None,       // 아무도 때리지 않을때
    Area,       // 범위 스킬일때
    Single,     // 단일 스킬일때
}


/// <summary>
/// 스킬의 디버프 타입을 나타냅니다.
/// </summary>
public enum SkillDebuffTypes
{
    None,       // 아무 디버프도 없는 스킬일때
    Stun,      // 기절을 거는 스킬일때
    Slow,       // 둔화를 거는 스킬일때
    Both        // 둔화 , 기절 둘다 거는 스킬일때
}

public enum SkillDamageType
{
    Instant,    // 즉발 스킬일때
    Dot,        // 도트 스킬일때
    Aoe         // 장판 스킬일때
}