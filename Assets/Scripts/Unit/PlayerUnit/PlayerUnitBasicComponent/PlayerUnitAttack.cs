using System.Collections;
using System.Collections.Generic;
using UnityEditor.Experimental.GraphView;
using UnityEngine;

public class PlayerUnitAttack : MonoBehaviour
{
    public EnemyUnit _TargetUnit;

    private List<EnemyUnit> _DetectedEnemies = new();

    private PlayerUnitMovement _PlayerUnitMovement;

    private PlayerUnit _PlayerUnit;

    private PlayerUnitSkillManager _PlayerUnitSkillManager;

    private BoxCollider _BoxCollider;


    public bool _IsAttack;

    public PlayerUnitMovement playerUnitMovement =>
        _PlayerUnitMovement ?? (_PlayerUnitMovement = GetComponent<PlayerUnitMovement>());

    public PlayerUnit playerUnit =>
        _PlayerUnit ?? (_PlayerUnit = GetComponent<PlayerUnit>());

    public PlayerUnitSkillManager skillManager =>
        _PlayerUnitSkillManager ?? (_PlayerUnitSkillManager = GetComponent<PlayerUnitSkillManager>());

    private void Awake()
    {
        _BoxCollider = GetComponent<BoxCollider>();
    }

    private void Update()
    {
        if (_TargetUnit == null)
            CheckAttackableArea();

        else
        {
            RotateToTarget();
            AttackToTarget();
        }
    }

    private void CheckAttackableArea()
    {
        Debug.Log("적 감지중");
        _DetectedEnemies.Clear();

        foreach (Collider detectedCollider in Physics.OverlapBox(transform.position, _BoxCollider.size))
        {
            Debug.Log("적 찾는중");

            if (detectedCollider.CompareTag(Constants.TAG_ENEMY))
            {
                EnemyUnit enemy = detectedCollider.GetComponent<EnemyUnit>();
                _DetectedEnemies.Add(enemy);
                Debug.Log("거리 안에 들어온 적 발견");
            }
        }

        if (_DetectedEnemies.Count > 1)
        {
            // 거리에 따라 정렬합니다.
            _DetectedEnemies.Sort((e1, e2) =>
            {
                Debug.Log("거리에 따라 정렬중");
                Vector3 playerCharacterPosition = transform.position;
                Vector3 e1Position = e1.transform.position;
                Vector3 e2Position = e2.transform.position;

                return Vector3.Distance(e1Position, playerCharacterPosition) < Vector3.Distance(e2Position, playerCharacterPosition) ? -1 : 1;
            });
        }

        if (_DetectedEnemies.Count > 0)
        {
            // 공격 타깃을 얻습니다.
            _TargetUnit = _DetectedEnemies[0].GetComponent<EnemyUnit>();
            Debug.Log("공격 타깃 찾음");
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject == _TargetUnit.gameObject)
            _TargetUnit = null;
    }

    private void AttackToTarget()
    {
        int randomPercent = Random.Range(0, 100);

        _TargetUnit.m_CurrentHp -= playerUnit.unitData.m_Atk;

        foreach (SkillData skill in playerUnit._SkillDataList)
        {
            if (skill.m_Percent.CheckPercent(randomPercent))
            {
                ActivateSkill(skill);
            }
        }
    }

    private void RotateToTarget()
    {

        // 회전시킬 방향 계산
        Vector3 direction = _TargetUnit.transform.position - transform.position;

        // 회전값 계산
        Quaternion targetRotation = Quaternion.LookRotation(direction);
        targetRotation.x = 0.0f;
        targetRotation.z = 0.0f;

        // 회전
        transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, playerUnitMovement.m_RotateSpeed);

    }


    public void ActivateSkill(SkillData skill)
    {
        #region Area Skill
        if (skill.m_TargetTypes == TargetType.Area)
        {
            switch (skill.m_SkillDebuffTypes)
            {
                case SkillDebuffTypes.None:
                    skillManager.Skill_Area_Damage(playerUnit, skill);
                    break;
                case SkillDebuffTypes.Stun:
                    skillManager.Skill_Area_Stun(playerUnit, skill);
                    break;
                case SkillDebuffTypes.Slow:
                    skillManager.Skill_Area_Slow(playerUnit, skill);
                    break;
                case SkillDebuffTypes.Both:
                    skillManager.Skill_Area_Slow(playerUnit, skill);
                    skillManager.Skill_Area_Stun(playerUnit, skill);
                    break;
                default:
                    break;
            }
        }
        #endregion

        else
        {
            switch (skill.m_SkillDebuffTypes)
            {
                case SkillDebuffTypes.None:
                    skillManager.Skill_Single_Damage(playerUnit, skill);
                    break;
                case SkillDebuffTypes.Stun:
                    skillManager.Skill_Single_Stun(playerUnit, skill);
                    break;
                case SkillDebuffTypes.Slow:
                    skillManager.Skill_Single_Slow(playerUnit, skill);
                    break;
                case SkillDebuffTypes.Both:
                    skillManager.Skill_Single_Slow(playerUnit, skill);
                    skillManager.Skill_Single_Stun(playerUnit, skill);
                    break;
                default: break;
            }
        }
    }
}
