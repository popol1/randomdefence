using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerUnitAnim : AnimController
{

    public const string PARAMNAME_ISDETECTED = "_IsDetected";

    /// <summary>
    /// 기본 공격시 발생하는 이벤트
    /// </summary>
    public event Action onBasicAttack;






    public void Anim_Event_BasicAttack() 
        => onBasicAttack?.Invoke();
}
