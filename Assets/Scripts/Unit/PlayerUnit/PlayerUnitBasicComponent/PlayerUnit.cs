using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public sealed class PlayerUnit : UnitBase
{

    /// <summary>
    /// 유닛의 등급입니다.
    /// </summary>
    public UnitTech m_UnitTech;

    /// <summary>
    /// 유닛이 가지고 있을 스킬 코드 목록입니다
    /// </summary>
    public string[] m_SkillCodes;

    /// <summary>
    /// PlayerUnitMovement 컴포넌트입니다
    /// </summary>
    private PlayerUnitMovement _PlayerUnitMovement;

    /// <summary>
    /// PlayerUnitAttack 컴포넌트입니다.
    /// </summary>
    private PlayerUnitAttack _PlayerUnitAttack;

    /// <summary>
    /// PlayerUnitAnim 컴포넌트입니다.
    /// </summary>
    private PlayerUnitAnim _PlayerUnitAnim;



    private static UnitCreateRecScriptableObject _UnitCreateRecList;

    public CreateRec _CreateRec;


    private static SkillDataScriptableObject _SkillDataScriptableObject;

    /// <summary>
    /// PlayerUnitMovement 읽기 전용 프로퍼티입니다
    /// </summary>
    public PlayerUnitMovement unitMovement =>
        _PlayerUnitMovement ?? (_PlayerUnitMovement = GetComponent<PlayerUnitMovement>());

    /// <summary>
    /// PlayerUnitAttack 읽기 전용 프로퍼티입니다.
    /// </summary>
    public PlayerUnitAttack unitAttack =>
        _PlayerUnitAttack ?? (_PlayerUnitAttack = GetComponent<PlayerUnitAttack>());

    /// <summary>
    /// PlayerUnitAnim 읽기 전용 프로퍼티입니다.
    /// </summary>
    public PlayerUnitAnim unitAnim =>
        _PlayerUnitAnim ?? (_PlayerUnitAnim = GetComponentInChildren<PlayerUnitAnim>());

    public List<SkillData> _SkillDataList { get; private set; }

    protected override void Awake()
    {
        base.Awake();

        if (m_UnitTech != UnitTech.Highest && m_UnitTech != UnitTech.Unused && m_UnitTech != UnitTech.Selectable)
        {
            if (_UnitCreateRecList == null)
                _UnitCreateRecList = Resources.Load<UnitCreateRecScriptableObject>(Constants.RESOURECES_PATH_UNITREC);
            _CreateRec = _UnitCreateRecList.GetCreateRec(m_ObjectCode);
        }

        if(m_UnitTech != UnitTech.Selectable)
        {
            if (_SkillDataScriptableObject == null)
                _SkillDataScriptableObject = Resources.Load<SkillDataScriptableObject>(Constants.RESOURCES_PATH_SKILLDATA);

            for(int i=0;i<m_SkillCodes.Length -1;i++)
            {
                _SkillDataList.Add(_SkillDataScriptableObject.GetSkillData(m_SkillCodes[i]));
            }
        }

    }


    private void Update()
    {
        UpdateAnimParamValues();
    }

    private void UpdateAnimParamValues()
    {

    }
}

