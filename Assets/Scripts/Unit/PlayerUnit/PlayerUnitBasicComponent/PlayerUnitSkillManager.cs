using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlayerUnitSkillManager : MonoBehaviour
{
    #region Area Skill
    public IEnumerator Skill_Area_Stun(PlayerUnit playerUnit ,SkillData skillStat)
    {
        Instantiate(skillStat.m_SkillEffect, playerUnit.unitAttack._TargetUnit.transform.position, Quaternion.identity);

        Collider[] cols = Physics.OverlapBox(playerUnit.transform.position, skillStat.m_Range.FloatToVector3(), Quaternion.identity);
        if (cols.IsArrayEmpty()) yield break;
    
        EnemyUnit[] enemies = cols.GetComponentsInColliders<EnemyUnit>();
        
        
        foreach (EnemyUnit enemy in enemies)
        {
            float originSpeed = enemy.enemyMovement.m_MaxMoveSpeed;
            enemy.enemyMovement.m_CurrentMoveSpeed = originSpeed * (1 - skillStat.m_SlowAmount / 100);
            enemy.ApplyDamage(skillStat.m_Damage);

        }
        yield return new WaitForSeconds(skillStat.m_DebuffTime);
    
        foreach (EnemyUnit enemy in enemies)
            enemy.enemyMovement.m_CurrentMoveSpeed = enemy.enemyMovement.m_MaxMoveSpeed;
    }

    public IEnumerator Skill_Area_Slow(PlayerUnit playerUnit, SkillData skillStat)
    {
        Instantiate(skillStat.m_SkillEffect, playerUnit.unitAttack._TargetUnit.transform.position, Quaternion.identity);
        Collider[] cols = Physics.OverlapBox(playerUnit.transform.position, skillStat.m_Range.FloatToVector3(), Quaternion.identity);
        if (cols.IsArrayEmpty()) yield break;
    
        EnemyUnit[] enemies = cols.GetComponentsInColliders<EnemyUnit>();
    
    
        foreach (EnemyUnit enemy in enemies)
        {
            float originSpeed = enemy.enemyMovement.m_MaxMoveSpeed;
            enemy.enemyMovement.m_CurrentMoveSpeed = originSpeed * (1 - skillStat.m_SlowAmount / 100);
            enemy.ApplyDamage(skillStat.m_Damage);

        }
        yield return new WaitForSeconds(skillStat.m_DebuffTime);
    
        foreach (EnemyUnit enemy in enemies)
            enemy.enemyMovement.m_CurrentMoveSpeed = enemy.enemyMovement.m_MaxMoveSpeed;
    }

    public void Skill_Area_Damage(PlayerUnit playerUnit, SkillData skillStat)
    {
        Instantiate(skillStat.m_SkillEffect, playerUnit.unitAttack._TargetUnit.transform.position, Quaternion.identity);
        Collider[] cols = Physics.OverlapBox(playerUnit.transform.position, skillStat.m_Range.FloatToVector3(), Quaternion.identity);
        if (cols.IsArrayEmpty()) return;

        EnemyUnit[] enemies = cols.GetComponentsInColliders<EnemyUnit>();

        foreach(EnemyUnit enemy in enemies)
        {
            enemy.ApplyDamage(skillStat.m_Damage);
        }
    }

    #endregion

    #region Single Skill
    public IEnumerator Skill_Single_Slow(PlayerUnit playerUnit, SkillData skillStat)
    {
        Instantiate(skillStat.m_SkillEffect, playerUnit.unitAttack._TargetUnit.transform.position, Quaternion.identity);
        EnemyUnit enemy = playerUnit.unitAttack._TargetUnit;
        if (enemy == null) yield break;

        enemy.ApplyDamage(skillStat.m_Damage);
        float originSpeed = enemy.enemyMovement.m_MaxMoveSpeed;
        enemy.enemyMovement.m_MaxMoveSpeed = enemy.enemyMovement.m_MaxMoveSpeed * (1 - skillStat.m_SlowAmount / 100);
    
        yield return new WaitForSeconds(skillStat.m_DebuffTime);
        enemy.enemyMovement.m_MaxMoveSpeed = originSpeed;
    }

    public IEnumerator Skill_Single_Stun(PlayerUnit playerUnit, SkillData skillStat)
    {
        Instantiate(skillStat.m_SkillEffect, playerUnit.unitAttack._TargetUnit.transform.position, Quaternion.identity);
        EnemyUnit enemy = playerUnit.unitAttack._TargetUnit;
        if (enemy == null) yield break;

        enemy.ApplyDamage(skillStat.m_Damage);
        enemy.m_IsAllowMove = false;
        yield return new WaitForSeconds(skillStat.m_DebuffTime);
    
        enemy.m_IsAllowMove = true;
    }

    public void Skill_Single_Damage(PlayerUnit playerUnit, SkillData skillStat)
    {
        Instantiate(skillStat.m_SkillEffect, playerUnit.unitAttack._TargetUnit.transform.position, Quaternion.identity);
        EnemyUnit enemy = playerUnit.unitAttack._TargetUnit;
        if (enemy == null) return;
        enemy.ApplyDamage(skillStat.m_Damage);

    }
    #endregion

    #region Aura Skill
    public void Skill_Aura_Slow()
    {

    }

    public void Skill_Aura_LowerDefense()
    {

    }
    #endregion
}