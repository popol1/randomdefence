using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerUnitMovement : MonoBehaviour
{
    public float m_RotateSpeed = 3;

    public float m_MoveSpeed;

    public Vector3 m_TargetPos;

    public bool m_IsMoving;

    private void Update()
    {
        MoveToPoint();
    }

    private void MoveToPoint()
    {
        if (!m_IsMoving) return;
        Vector3 pos = m_TargetPos;
        pos.y = 0f;

        transform.position = Vector3.MoveTowards(transform.position, pos, m_MoveSpeed * Time.deltaTime);
        RotateToPoint();
        if (Vector3.Distance(transform.position, pos) < 0.1f)
            m_IsMoving = false;
    }

    private void RotateToPoint()
    {
        Debug.Log("회전중");
        // 회전시킬 방향 계산
        Vector3 direction = m_TargetPos - transform.position;

        // 회전값 계산
        Quaternion targetRotation = Quaternion.LookRotation(direction);
        targetRotation.z = 0.0f;
        targetRotation.x = 0.0f;

        // 회전
        transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, m_RotateSpeed);
    }
}
