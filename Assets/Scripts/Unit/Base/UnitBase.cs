using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitBase : ObjectBase
{
    /// <summary>
    /// 현재 움직일수 있는지 나타냅니다.
    /// </summary>
    public bool m_IsAllowMove = true;

    public float m_CurrentHp;

    protected static UnitStatDataScriptableObject m_UnitDataScriptableObject;

    [SerializeField]
    private UnitStatData _UnitData;

    public UnitStatData unitData => _UnitData; 



    protected override void Awake()
    {
        base.Awake();
        if (m_UnitDataScriptableObject == null)
            m_UnitDataScriptableObject = Resources.Load<UnitStatDataScriptableObject>(Constants.RESOURECES_PATH_UNITDATA);

        _UnitData = m_UnitDataScriptableObject.GetUnitData(m_ObjectCode);
    }
}
