using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectBase : MonoBehaviour
{

    /// <summary>
    /// 유닛이 선택될때 표시될 오브젝트입니다.
    /// </summary>
    [Header("# 유닛 선택 오브젝트")]
    public GameObject m_SelectedObject;

    [Header("# 유닛 코드")]
    public string m_ObjectCode;

    /// <summary>
    /// 유닛이 선택됐는지 나타냅니다.
    /// </summary>
    public bool m_IsSelected;

    /// <summary>
    /// 적 정보를 담는 ScriptableObject 입니다.
    /// </summary>
    protected static ObjectStatDataScriptableObject _ObejctDataScriptableObject;

    [SerializeField]
    private ObjectData _ObjectData;

    public ObjectData objectData => _ObjectData;

    protected virtual void Awake()
    {
        if (_ObejctDataScriptableObject == null)
            _ObejctDataScriptableObject = Resources.Load<ObjectStatDataScriptableObject>(Constants.RESOURECES_PATH_OBJECTDATA);

        _ObjectData = _ObejctDataScriptableObject.GetObjectData(m_ObjectCode);
    }

    public void SelectUnit()
    {
        m_IsSelected = true;
        m_SelectedObject.SetActive(true);
    }

    public void DeSelectUnit()
    {
        m_IsSelected = false;
        m_SelectedObject.SetActive(false);
    }
}

