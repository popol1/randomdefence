using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyUnitMovement : MonoBehaviour
{
    /// <summary>
    /// 몬스터가 이동할 목적지의 부모 객체입니다.
    /// </summary>
    [Header("# 몬스터가 이동할 웨이포인트 목록의 부모")]
    public GameObject m_WayPointTransform;

    /// <summary>
    /// 유닛의 이동속도
    /// </summary>
    public float m_MaxMoveSpeed;

    public float m_CurrentMoveSpeed;

    /// <summary>
    /// 유닛의 회전속도
    /// </summary>
    public float m_RotationSpeed = 5.0f;

    /// <summary>
    /// EnemyUnit 컴포넌트입니다.
    /// </summary>
    private EnemyUnit _EnemyUnit;

    /// <summary>
    /// 목적지 목록입니다.
    /// </summary>
    private Transform[] _WayPoints;

    /// <summary>
    /// 현재 목적지의 인덱스입니다.
    /// </summary>
    private int currentWayPointIndex = 0;

    /// <summary>
    /// EnemyUnit의 읽기 전용 프로퍼티입니다.
    /// </summary>
    public EnemyUnit enemyUnit => _EnemyUnit ?? (_EnemyUnit = GetComponent<EnemyUnit>());

    private void Start()
    {
        List<Transform> transforms = new (m_WayPointTransform.GetComponentsInChildren<Transform>());
        transforms.Remove(m_WayPointTransform.transform);

        _WayPoints = transforms.ToArray();
    }

    private void Update()
    {
        OnMoveToWayPoint();
    }

    /// <summary>
    /// 목적지를 향해 움직입니다.
    /// </summary>
    private void OnMoveToWayPoint()
    {
        // 유닛이 움직일수 없는 상태라면 함수를 종료합니다.
       // if (!enemyUnit.m_IsAllowMove) return;

        // 목적지를 향해 움직입니다.
        transform.position = Vector3.MoveTowards(
            gameObject.transform.position, _WayPoints[currentWayPointIndex].position, m_MaxMoveSpeed * Time.deltaTime);

        // 목적지를 향해 바라보도록 오브젝트를 회전시킵니다.
        RotateToWayPoint();

        // 만약 목적지에 도착했다면 다음 웨이 포인트를 설정합니다.
        if (CalculateDistanceWayPoint())
            currentWayPointIndex++;

        // 마지막 목적지에 도착했다면 처음 웨이포인트로 설정합니다.
        if (currentWayPointIndex >= _WayPoints.Length)
            currentWayPointIndex = 0;
    }

    /// <summary>
    /// 목적지 향해 회전합니다.
    /// </summary>
    private void RotateToWayPoint()
    {
        // 회전시킬 방향 계산
        Vector3 direction = _WayPoints[currentWayPointIndex].position - transform.position;

        // 회전값 계산
        Quaternion targetRotation = Quaternion.LookRotation(direction);

        // 회전
        transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation , m_MaxMoveSpeed);
    }

    /// <summary>
    /// 목적지에 도착했는지 거리를 확인합니다.
    /// </summary>
    /// <returns></returns>
    private bool CalculateDistanceWayPoint()
    {
        // 현재 오브젝트의 위치
        Vector3 currentPos = transform.position;
        currentPos.y = 0.0f;

        // 목적지의 위치
        Vector3 wayPointPos = _WayPoints[currentWayPointIndex].position;
        wayPointPos.y = 0.0f;

        // 오브젝트의 위치와 목적지의 위치 거리 비교
        bool result = Vector3.Distance(currentPos, wayPointPos) < 0.1f;

        return result;
    }

}
