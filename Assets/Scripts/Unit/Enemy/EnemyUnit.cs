using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyUnit : UnitBase
{

    /// <summary>
    /// EnemyUnitMovement 컴포넌트입니다.
    /// </summary>
    private EnemyUnitMovement _EnemyUnitMovement;

    /// <summary>
    /// EnemyUnitMovement 의 읽기 전용 프로퍼티입니다. 
    /// </summary>
    public EnemyUnitMovement enemyMovement =>
        _EnemyUnitMovement ?? (_EnemyUnitMovement = GetComponent<EnemyUnitMovement>());



    public void ApplyDamage(float damage)
    {
        m_CurrentHp -= unitData.m_Def - damage;
    }

}
