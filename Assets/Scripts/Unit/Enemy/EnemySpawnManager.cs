using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class EnemySpawnManager : MonoBehaviour
{
    /// <summary>
    /// 적 유닛을 생성시킬 위치입니다.
    /// </summary>
    [Header("# 적 스폰 위치")]
    public GameObject m_EnemySpawnPos;

    public GameObject m_EnemyWayPointTransform;

    /// <summary>
    /// 생성시킬 변수입니다.
    /// </summary>
    private int _MaxSpawnCount = 10;

    /// <summary>
    /// 현재 유닛의 카운트입니다
    /// </summary>
    private int _CurrentUnitCount = 0;

    /// <summary>
    /// 생성 쿨타임입니다.
    /// </summary>
    private float _SpawonCoolTime = 1.0f;

    /// <summary>
    /// 생성시킬 유닛 리스트의 인덱스입니다.
    /// </summary>
    private int _SpawnUnitIndex = 0;

    /// <summary>
    /// 몬스터 스폰의 가능여부입니다.
    /// </summary>
    public bool _IsAllowSpawn;
    /// <summary>
    /// 소환시킬 적 유닛의 목록입니다.
    /// </summary>
    public List<EnemyUnit> m_EnemyUnits;

    private void Start()
    {
        StartCoroutine(SpawnUnit());
    }

    /// <summary>
    /// 몬스터를 생성시키는 코루틴입니다.
    /// </summary>
    /// <returns></returns>
    private IEnumerator SpawnUnit()
    {
        while (true)
        {
            // 스폰이 가능할때까지 대기
            yield return new WaitUntil(() => _IsAllowSpawn == true);

            // 보스 출현 라운드가 아닐때까지 대기
            yield return new WaitUntil(() => RoundManager.instance.currentRound % 10 != 0);

            // 몬스터 생성
            EnemyUnit enemyUnit =
                Instantiate(m_EnemyUnits[_SpawnUnitIndex].gameObject, m_EnemySpawnPos.transform.position, Quaternion.identity)
                .GetComponent<EnemyUnit>();

            // 몬스터의 목적지 설정
            enemyUnit.enemyMovement.m_WayPointTransform = m_EnemyWayPointTransform;
            
            _CurrentUnitCount++;
            RoundManager.instance.currentMonsterCount++;

            // 몬스터를 최대치만큼 생성시키면 생성 중단하고 다음 몬스터 생성 준비
            if( _CurrentUnitCount == _MaxSpawnCount )
            {
                _CurrentUnitCount = 0;
                _SpawnUnitIndex++;
                _IsAllowSpawn = false;
            }

            // 쿨타임만큼 대기
            yield return new WaitForSecondsRealtime(_SpawonCoolTime);
        }
    }

    
}
