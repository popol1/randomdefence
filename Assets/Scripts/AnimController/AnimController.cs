using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimController : MonoBehaviour
{
    public Animator animator { get; protected set; }

    protected virtual void Awake()
        => animator = GetComponent<Animator>();

    public void SetFloat(string paramName, float newValue) => animator.SetFloat(paramName, newValue);
    public void SetInt(string paramName, int newValue) => animator.SetInteger(paramName, newValue);
    public void SetBool(string paramName, bool newValue) => animator.SetBool(paramName, newValue);
    public void SetTrigger(string paramName) => animator.SetTrigger(paramName);

    public float GetFloat(string paramName) => animator.GetFloat(paramName);
    public int GetInt(string paramName) => animator.GetInteger(paramName);
    public bool GetBool(string paramName) => animator.GetBool(paramName);
}
