

public static class Constants
{
    #region Colider Tag
    /// <summary>
    /// PlayerUnit 태그 이름입니다.
    /// </summary>
    public const string TAG_PLAYER_UNIT = "PlayerUnit";

    public const string TAG_ENEMY = "Enemy";

    public const string TAG_FLOOR = "Floor";
    #endregion

    #region Resources Path
    public const string RESOURECES_PATH_UNITREC =       "ScriptableObject/UnitCreateRecList";
    public const string RESOURECES_PATH_OBJECTDATA =    "ScriptableObject/ObjectDataList";
    public const string RESOURECES_PATH_UNITDATA =      "ScriptableObject/UnitDataList";
    public const string RESOURCES_PATH_SKILLDATA =      "ScriptableObject/SkillData";
    #endregion

    #region Unit Count
    public const int MODE_EASY_UNITCOUNT = 50;
    public const int MODE_NORMAL_UNITCOUNT = 45;
    public const int MODE_HARD_UNITCOUNT = 40;

    #endregion


    public const int DEFAULT_ROUNDTIME = 40;
}