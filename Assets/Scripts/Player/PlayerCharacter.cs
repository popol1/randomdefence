using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCharacter : PlayerCharacterBase
{
    private PlayerMovement _PlayerMovement;

    private PlayerUnitList _PlayerUnitList;

    public PlayerMovement playerMovement => _PlayerMovement ?? (_PlayerMovement = GetComponent<PlayerMovement>());

    public PlayerUnitList playerUnitList => _PlayerUnitList ?? (_PlayerUnitList = GetComponent<PlayerUnitList>());

    public ObjectBase SelectedObject { get; private set; }


    public int m_Gold;

    public int m_Wood;

    public int m_LevelPoint;

    private void Awake()
    {
        SelectedObject = playerMovement._SelectedObject;
    }

    private void Start()
    {
        InitializedPlayerStatePanel();
    }

    private void InitializedPlayerStatePanel()
    {
        GameSceneInstance gameSceneInstance = SceneManagerBase.instance.GetSceneInstance<GameSceneInstance>();
        gameSceneInstance.m_GameUI.resourcePanel.SetResourceText(m_Gold,m_Wood,m_LevelPoint);
    }

    public void OnSelectedInput()
        => playerMovement?.OnSelectedInput();

    public void OnSetMovepointInput()
        => playerMovement?.OnSetMovepointInput();

    public void OnCreateUnitInput()
        => playerUnitList?.UnitCreate();

    public void OnDragSelectEndInput()
        => playerMovement?.EndDragSelection();

    public void OnDragSelectingInput()
     => playerMovement?.OnDragSelecting();

    public void OnAddUnitDic(string unitCode , PlayerUnit playerUnit)
    {
        playerUnitList.unitCodes.Add(unitCode);
        playerUnitList.units.Add(playerUnit);
    }



}
