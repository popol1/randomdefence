using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;


public class PlayerMovement : MonoBehaviour
{
    /// <summary>
    /// 현재 선택된 유닛을 나타냅니다.
    /// </summary>
    public ObjectBase _SelectedObject;

    public List<PlayerUnit> _SelectedUnitList;

    [SerializeField]
    private RectTransform _DragRectangle;

    private Rect _DragRect;

    private Vector2 start;

    private Vector2 end;

    private PlayerUnitList _UnitList;

    public PlayerUnitList playerUnitList =>
        _UnitList ?? (_UnitList = GetComponent<PlayerUnitList>());

    public GameObject StartingDebugObject;
    public GameObject EndDebugObject;


    private bool _IsDraging;
    private void OnInitializedUnitUI()
    {
        GameSceneInstance gameSceneInstance = SceneManagerBase.instance.GetSceneInstance<GameSceneInstance>();
        gameSceneInstance.m_GameUI.unitStatePanel.SetUnitStatePanel(_SelectedUnitList[0]);
    }  
    private void OnInitializedUnitListUI()
    {
        GameSceneInstance gameSceneInstance = SceneManagerBase.instance.GetSceneInstance<GameSceneInstance>();
        gameSceneInstance.m_GameUI.unitStatePanel.SetUnitListStatePanel(_SelectedUnitList);
    }
    private void Update()
    {
        if (_IsDraging)
        {

            end = Mouse.current.position.ReadValue();

            DrawDragRectangle();
        }
    }

    /// <summary>
    /// 좌클릭시 호출되는 함수입니다.
    /// </summary>
    public void OnSelectedInput()
    {
        start = Mouse.current.position.ReadValue();
        _DragRect = new();

        #region 주석
        //if (EventSystem.current.IsPointerOverGameObject()) return;
        // 마우스 위치 받아옴
        //Vector3 mousePosition = Mouse.current.position.ReadValue();
        //
        //Ray ray = Camera.main.ScreenPointToRay(mousePosition);
        //
        //RaycastHit hitResult;
        //
        //if(Physics.Raycast(ray, out hitResult,Mathf.Infinity))
        //{
        //    if (hitResult.collider.CompareTag("Button"))
        //    {
        //        Debug.Log("버튼 클릭");
        //    }
        //
        //    if(hitResult.collider.CompareTag(Constants.TAG_PLAYER_UNIT))
        //    {
        //        if (_SelectedUnit != null)
        //            _SelectedUnit.m_IsSelected = false;
        //
        //
        //        _SelectedUnit = hitResult.transform.GetComponent<ObjectBase>();
        //        hitResult.transform.GetComponent<ObjectBase>().m_IsSelected = true;
        //
        //        OnInitializedUI();
        //    }    
        //} 
        #endregion
    }

    /// <summary>
    /// 드래그 중일때 호출되는 함수입니다.
    /// </summary>
    public void OnDragSelecting()
    {
        _IsDraging = true;
    }


    /// <summary>
    /// 드래그가 끝날때 호출되는 함수
    /// </summary>
    public void EndDragSelection()
    {
        // 유닛 선택 해제
        DeSelectAll();

        // 드래그 사각형 계산
        CalcDragRect();

        // 사각형 내부 유닛 선택
        DragSelectUnit();

        start = end = Vector2.zero;

        // 드래그 사각형 그림
        DrawDragRectangle();

        _IsDraging = false;

        if(_SelectedUnitList.Count == 1)
            OnInitializedUnitUI();

        else if(_SelectedUnitList.Count > 1)
        OnInitializedUnitListUI();
    }


    /// <summary>
    /// 우클릭시 선택된 유닛을 움직이도록 하는 함수입니다
    /// </summary>
    public void OnSetMovepointInput()
    {
        if (_SelectedUnitList.IsEmpty()) return;
        Vector3 mousePosition = Mouse.current.position.ReadValue();
        Ray ray = Camera.main.ScreenPointToRay(mousePosition);
        RaycastHit hitResult;

        if (Physics.Raycast(ray, out hitResult, Mathf.Infinity))
        {

            if (_SelectedUnitList.Count > 0)
            {
                foreach (PlayerUnit units in _SelectedUnitList)
                {
                    units.unitAttack._TargetUnit = null;
                    units.unitMovement.m_TargetPos = hitResult.point;
                    units.unitMovement.m_IsMoving = true;
                    if (hitResult.collider.CompareTag(Constants.TAG_ENEMY))
                        units.unitAttack._TargetUnit = hitResult.transform.GetComponent<EnemyUnit>();
                }

            }
        }
    }
    private void DragSelectUnit()
    {
        foreach (PlayerUnit unit in playerUnitList.units)
        {
            if (_DragRect.Contains(Camera.main.WorldToScreenPoint(unit.transform.position)))
            {
                SelectUnit(unit);
            }
        }
    }
    /// <summary>
    /// 드래그 사각형 이미지를 그립니다.
    /// </summary>
    private void DrawDragRectangle()
    {
        _DragRectangle.position = (start + end) * 0.5f;

        _DragRectangle.sizeDelta = new Vector2(Mathf.Abs(start.x - end.x), Mathf.Abs(start.y - end.y));
    }

    /// <summary>
    /// Drag Rect를 계산합니다.
    /// </summary>
    private void CalcDragRect()
    {
        if (Mouse.current.position.ReadValue().x < start.x)
        {
            _DragRect.xMin = Mouse.current.position.ReadValue().x;
            _DragRect.xMax = start.x;
        }
        else
        {
            _DragRect.xMin = start.x;
            _DragRect.xMax = Mouse.current.position.ReadValue().x;
        }

        if (Mouse.current.position.ReadValue().y < start.y)
        {
            _DragRect.yMin = Mouse.current.position.ReadValue().y;
            _DragRect.yMax = start.y;
        }
        else
        {
            _DragRect.yMin = start.y;
            _DragRect.yMax = Mouse.current.position.ReadValue().y;
        }
    }

    /// <summary>
    /// 유닛을 선택합니다.
    /// </summary>
    /// <param name="newUnit">선택 시킬 유닛을 받습니다.</param>
    private void SelectUnit(PlayerUnit newUnit)
    {
        if (_SelectedUnitList.Count > 12) return;
        Debug.Log("Selected Unit Code = " + newUnit.m_ObjectCode);
        newUnit.SelectUnit();
        _SelectedUnitList.Add(newUnit);
    }

    /// <summary>
    /// 선택된 유닛을 전부 선택 해제합니다.
    /// </summary>
    private void DeSelectAll()
    {
        foreach (PlayerUnit unit in _SelectedUnitList)
        {
            unit.DeSelectUnit();
        }
        _SelectedUnitList.Clear();
    }

}

