using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;



public class PlayerUnitList : MonoBehaviour
{
    /// <summary>
    /// 플레이어의 움직임 컴포넌트
    /// </summary>
    private PlayerMovement _PlayerMovement;

    /// <summary>
    /// _PlayerMovement 읽기 전용 프로퍼티
    /// </summary>
    public PlayerMovement playerMovement => _PlayerMovement ?? (_PlayerMovement = GetComponent<PlayerMovement>());

    /// <summary>
    /// 플레이어가 갖고있는 유닛들의 코드를 담는 리스트
    /// </summary>
    public List<string> unitCodes;

    /// <summary>
    /// 플레이어가 갖고있는 유닛들을 담는 리스트
    /// </summary>
    public List<PlayerUnit> units;

    /// <summary>
    /// 유닛을 조합합니다.
    /// </summary>
    public void UnitCreate()
    {
        if (playerMovement._SelectedUnitList.IsEmpty()) return;

        if (!CheckCreateRec())
        {
            Debug.Log("재료 유닛이 충분하지 않습니다.");
            return;
        }


        PlayerUnit unit = playerMovement._SelectedUnitList[0];

        foreach (PlayerUnit Ingredient in unit._CreateRec._Units)
        {

            Debug.Log("Igredient Code = " + Ingredient.m_ObjectCode);

            PlayerUnit removeTarget = units.Find(elem => elem == Ingredient);


            for (int i = units.Count - 1; i >= 0; --i)
            {
                if (units[i].m_ObjectCode == Ingredient.m_ObjectCode)
                {
                    Destroy(units[i].gameObject);
                    unitCodes.RemoveAt(i);
                    units.RemoveAt(i);
                }
            }
        }
        
        Instantiate(unit._CreateRec._Result);
    }

    /// <summary>
    /// 조합의 재료가 충분한지 확인합니다.
    /// </summary>
    /// <returns>재료가 있다면 True , 없다면 False 를 반환합니다.</returns>
    public bool CheckCreateRec()
    {
        if (playerMovement._SelectedUnitList.IsEmpty())
        {
            Debug.Log("유닛 가지고있지않음");
            return false;
        }


        PlayerUnit unit = playerMovement._SelectedUnitList[0];

        foreach (PlayerUnit createdUnit in unit._CreateRec._Units)
        {
            if (!unitCodes.Contains(createdUnit.m_ObjectCode))
                return false;
        }
        return true;
    }
}
