using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public class PlayerController : PlayerControllerBase
{

    public PlayerCharacter character => controlledCharacter as PlayerCharacter;

    private PlayerInput _PlayerInput;



    private void Awake()
    {
        _PlayerInput = GetComponent<PlayerInput>();    
        
    }

    public override void StartControlCharacter(PlayerCharacterBase controlCharacter)
        => base.StartControlCharacter(controlCharacter);

    private void OnSelect()
        => character?.OnSelectedInput();

    private void OnSetMovepoint()
        => character?.OnSetMovepointInput();

    private void OnCreateUnit()
        => character?.OnCreateUnitInput();
    

    private void OnDragSelectEnd()
        => character?.OnDragSelectEndInput();

    private void OnDragSelecting()
        => character?.OnDragSelectingInput();
}
